package com.opdar.athena.support.base;

import com.alibaba.fastjson.JSON;
import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.base.Interceptor;
import com.opdar.platform.core.session.ISessionManager;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.io.IOException;

/**
 * Created by shiju on 2017/1/24.
 */
public class CommonInterceptor implements Interceptor,ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public boolean before() {
        Context.getResponse().setCharacterEncoding("UTF-8");
        String token = (String) Context.getRequest().getSession().getAttribute("token");
        if(Context.getRequest().getParameterMap().containsKey("token")){
            token = Context.getRequest().getParameter("token");
        }
        if(token != null){
            ISessionManager sessionManager = applicationContext.getBean(ISessionManager.class);
            Object user = sessionManager.get(token);
            if(user != null){
                sessionManager.clearTimeout(token);
                Context.putAttribute("user",user);
                return true;
            }
        }
        try {
            Context.getResponse().getOutputStream().write(JSON.toJSONBytes(Result.valueOf(100,"你好像太久没操作了")));
            Context.getResponse().getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean after() {
        return true;
    }

    @Override
    public void finish() {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
