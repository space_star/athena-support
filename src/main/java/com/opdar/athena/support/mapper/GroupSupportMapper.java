package com.opdar.athena.support.mapper;

import com.opdar.athena.support.entities.GroupEntity;
import com.opdar.athena.support.entities.GroupSupportEntity;
import com.opdar.plugins.mybatis.core.IBaseMapper;

/**
 * Created by shiju on 2017/8/2.
 */
public interface GroupSupportMapper extends IBaseMapper<GroupSupportEntity> {
}
