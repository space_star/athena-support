package com.opdar.athena.support.controllers.support;

import com.opdar.athena.support.base.CommonInterceptor;
import com.opdar.athena.support.base.Constants;
import com.opdar.athena.support.base.ICacheManager;
import com.opdar.athena.support.base.Result;
import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by shiju on 2017/7/13.
 */
@Controller
@Interceptor(CommonInterceptor.class)
public class SupportController {

    @Autowired
    ICacheManager<String, String, Object> cacheManager;

    @Request(value = "/support/state/update", format = Request.Format.JSON)
    public Result supportStateUpdate(Integer state) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        cacheManager.hset(Constants.USER_STATE, user.getId(), state);
        return Result.valueOf(null);
    }

    @Request(value = "/support/state/get", format = Request.Format.JSON)
    public Result supportStateGet() {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        Object state = cacheManager.hget(Constants.USER_STATE, user.getId());
        return Result.valueOf(state);
    }

}
